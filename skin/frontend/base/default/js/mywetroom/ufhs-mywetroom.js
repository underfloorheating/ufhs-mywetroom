var mywetroomOutstandingBasket = function(){
	location.reload(true);
}

$j(document).ready(function(){
	$j('[data-ajax="mywetroomOutstandingBasket"]').click(function(){
		$j.removeCookie('mywetroom-showmessage');
	});
	$j('[data-name="outstandingBasket"] .close').click(function(){
		$j('[data-name="outstandingBasket"]').addClass('hidden');
		$j.removeCookie('mywetroom-showmessage');
	});
	$j('[data-ajax]').click(function(e){
		e.preventDefault();
		$elem = $j(this);
		if(typeof window[$elem.data('ajax')] != "undefined"){
			var callback = window[$elem.data('ajax')]();
		}
		$j.ajax({
			url: $elem.attr('href')
		}).done(function(){
			if(typeof callback == "function"){
				callback();
			}
		});
	});
})