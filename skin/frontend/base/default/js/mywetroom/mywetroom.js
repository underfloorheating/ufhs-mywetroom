/**
 * String Format
 * -------------
 * Equivalent of sprintf in PHP
 */
 String.prototype.format = function() {
 	var str = this,
 	i = arguments.length;

 	while (i--) {
 		str = str.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
 	}
 	return str;
 };


 (function($){

 	var cookieClass = function(key, value){
 		this.key = key;
 		this.value = value;
 		this.set = function(data){
 			$.cookie(this.key,JSON.stringify(data));
 		};
 		this.unset = function(){
 			$.removeCookie(this.key);
 		};
 		this.get = function(){
 			return JSON.parse($.cookie(this.key))
 		};
 		this.doIExist = function(){
 			return typeof $.cookie(this.key) != "undefined";
 		};
 	};

 	mywetroom = {
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ///////////////////////////////////////////////////// STATIC VARIABLES /////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 url: {
		 	baseUrl: '/mywetroom/product/',
		 	productLookup: 'productlookup',
		 	matLookup: 'matlookup',
		 	addBasket: 'addtobasket',
		 	imageLookup: 'filenamelookup',
		 	imgSrc: '/skin/frontend/base/default/images/mywetroom/',
		 	unsavedKit: 'outstandingbasket'
		 },
		 selectedItems: {},
		 errorTitles: {
		 	"drain": "Sorry, it's not compatible with your chosen drain.",
		 	"size": "Sorry, it's not available in your chosen size.",
		 	"placement": "Sorry, it's not available with your chosen placement.",
		 	"thickness": "Sorry, it's not available in your chosen thickness."
		 },
		 errorMessages: {
		 	"size": "The {0} is not available in the {1} size you have selected.",
		 	"placement": "The {0} is not available with the {1} drain placement you have selected.",
		 	"drain": "The {0} doesn't fit the {1} drain you have selected.",
		 	"thickness": "The {0} is not available in the {1} thickness you have selected.",
		 },
		 errorLookupObject: {
		 	'20': {
		 		'type': ["20mm tray"],
		 		'drain': {
		 			"standard": "standard",
		 			"stainless": "stainless steel",
		 			"tileable": "tileable",
		 			"linearStainless": "linear stainless steel",
		 			"linearTileable": "tileable linear"
		 		},
		 		'size': {
		 			'1500x800': '1500mm x 800mm',
		 			'1600x900': '1600mm x 900mm',
		 			'1200x1200': '1200mm x 1200mm',
		 			'1850x900': '1850mm x 900mm',
		 			'1400x900': '1400mm x 900mm'
		 		},
		 		'placement': {
		 			'end': 'end',
		 			'linearCentre': 'linear centre',
		 			'linearEnd': 'linear end'
		 		}
		 	},
		 	'centre': {
		 		'type': ["centre drain placement option"],
		 		'size': {
		 			'1400x900': '1400mm x 900mm'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'corner': {
		 		'type': ["corner drain placement option"],
		 		'size': {
		 			'1500x800': '1500mm x 800mm',
		 			'1600x900': '1600mm x 900mm',
		 			'1200x900': '1200mm x 900mm',
		 			'1850x900': '1850mm x 900mm',
		 			'1400x900': '1400mm x 900mm'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'end': {
		 		'type': ["end drain placement option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'size': {
		 			'800x800': '800mm x 800mm',
		 			'900x900': '900mm x 900mm',
		 			'1000x1000': '1000mm x 1000mm',
		 			'1200x1200': '1200mm x 1200mm',
		 			'1850x900': '1850mm x 900mm',
		 			'1400x900': '1400mm x 900mm'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'linearCentre': {
		 		'type': ["linear drain placement option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'size': {
		 			'800x800': '800mm x 800mm',
		 			'1000x1000': '1000mm x 1000mm',
		 			'1500x800': '1500mm x 800mm',
		 			'1200x1200': '1200mm x 1200mm',
		 			'1850x900': '1850mm x 900mm'
		 		},
		 		'drain': {
		 			'standard': 'standard',
		 			'stainless': 'stainless steel',
		 			'tileable': 'tileable'
		 		}
		 	},
		 	'linearEnd': {
		 		'type': ["linear end drain placement option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'size': {
		 			'800x800': '800mm x 800mm',
		 			'1000x1000': '1000mm x 1000mm',
		 			'1500x800': '1500mm x 800mm',
		 			'1200x1200': '1200mm x 1200mm',
		 			'1850x900': '1850mm x 900mm'
		 		},
		 		'drain': {
		 			'standard': 'standard',
		 			'stainless': 'stainless steel',
		 			'tileable': 'tileable'
		 		}
		 	},
		 	'800x800': {
		 		'type': ["800mm x 800mm tray size"],
		 		'placement': {
		 			'end': 'end',
		 			'linearEnd': 'linear end',
		 			'linearCentre': 'linear centre'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'900x900': {
		 		'type': ["900mm x 900mm tray size"],
		 		'placement': {
		 			'end': 'end'
		 		}
		 	},
		 	'1000x1000': {
		 		'type': ["1000mm x 1000mm tray size"],
		 		'placement': {
		 			'end': 'end',
		 			'linearEnd': 'linear end',
		 			'linearCentre': 'linear centre'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'1200x900': {
		 		'type': ["1200mm x 900mm tray size"],
		 		'placement': {
		 			'corner': 'corner'
		 		}
		 	},
		 	'1500x800': {
		 		'type': ["1500mm x 800mm tray size"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner',
		 			'linearEnd': 'linear end',
		 			'linearCentre': 'linear centre',
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'1600x900': {
		 		'type': ["1600mm x 900mm tray size"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner'
		 		}
		 	},
		 	'1200x1200': {
		 		'type': ["1200mm x 1200mm tray size"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'end': 'end',
		 			'linearEnd': 'linear end',
		 			'linearCentre': 'linear centre'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'1850x900': {
		 		'type': ["1850mm x 900mm tray size"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner',
		 			'end': 'end',
		 			'linearEnd': 'linear end',
		 			'linearCentre': 'linear centre'
		 		},
		 		'drain': {
		 			'linearStainless': 'linear stainless steel',
		 			'linearTileable': 'linear tileable'
		 		}
		 	},
		 	'1400x900': {
		 		'type': ["1400mm x 900mm tray size"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner',
		 			'end': 'end',
		 			'centre': 'centre'
		 		}
		 	},
		 	'standard': {
		 		'type': ["standard drain option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'linearCentre': 'linear centre',
		 			'linearEnd': 'linear end'
		 		}
		 	},
		 	'stainless': {
		 		'type': ["stainless steel drain option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'linearCentre': 'linear centre',
		 			'linearEnd': 'linear end'
		 		}
		 	},
		 	'tileable': {
		 		'type': ["tileable drain option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'linearCentre': 'linear centre',
		 			'linearEnd': 'linear end'
		 		}
		 	},
		 	'linearStainless': {
		 		'type': ["linear stainless steel drain option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner',
		 			'end': 'end',
		 			'centre': 'centre'
		 		}
		 	},
		 	'linearTileable': {
		 		'type': ["linear tileable drain option"],
		 		'thickness': {
		 			'20': '20mm'
		 		},
		 		'placement': {
		 			'corner': 'corner',
		 			'end': 'end',
		 			'centre': 'centre'
		 		}
		 	},
		 },
		 stepIndex: {
			//This needs to be made dynamically
			one: 0,
			two: 1,
			three: 2,
			four: 3,
			five: 4,
			six: 5
		},
		widthRoom: 0,
		lengthRoom: 0,
		heightRoom: 0,
		showMessage: false,
		kitItems: [
		],
		kitTotal: 0.00,
		unlockedSteps: {
		},
		completedSteps: {
		},
		outputtedImages: {
		},
		selectedProducts: {
		},
		stepUnlockMap: {
			'one': ['two','three','four','five','six'],
			'two': ['three'],
			'three': ['four','five','six'],
			'four': ['five','six'],
			'five': ['six']
		},
		stepImageMapping: {
			//This needs to be made dynamically
			'one': 'tray',
			'two': 'drain',
			'three': 'board',
			'floor': 'board',
			'wall': 'board',
			'four': 'install',
			'five': 'screen',
			'six': 'mat'
		},
		cookiesArray: [
		{
			id: 'kititems',
			key: 'mywetroom-kititems',
			value: 'kitItems'
		},
		{
			id: 'options',
			key: 'mywetroom-options',
			value: 'selectedItems'
		},
		{
			id: 'selections',
			key: 'mywetroom-selections',
			value: 'selectedProducts'
		},
		{
			id: 'unlocked',
			key: 'mywetroom-unlocked',
			value: 'unlockedSteps'
		},
		{
			id: 'completed',
			key: 'mywetroom-completed',
			value: 'completedSteps'
		},
		{
			id: 'pictures',
			key: 'mywetroom-pictures',
			value: 'outputtedImages'
		},
		{
			id: 'total',
			key: 'mywetroom-total',
			value: 'kitTotal'
		},
		{
			id: 'length',
			key: 'mywetroom-length',
			value: 'lengthRoom'
		},
		{
			id: 'width',
			key: 'mywetroom-width',
			value: 'widthRoom'
		},
		{
			id: 'height',
			key: 'mywetroom-height',
			value: 'heightRoom'
		},
		{
			id: 'showmessage',
			key: 'mywetroom-showmessage',
			value: 'showMessage'

		}
		],
		cookiesObjs: {},
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ////////////////////////////////////////////////////// INITIALISATION //////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 init: function(){
		 	var app = this;
		 	this.getHandles();
		 	this.initCookies();
		 	this.initRoomDimensions();
		 	this.initPrices();
		 	this.bindInputValidation();
		 	this.setDisplayHeight();
		 	this.initAccordion();
		 	this.initOptions();
		 	this.initTooltip();
		 	this.initBoardCal();
		 	this.initPopups();
		 	this.initAddToKits();
		 	this.initKitItems();
		 	this.initMatProduct();
		 	this.initContinueButton();
		 	this.restoreSession();
		 	this.cookiesObjs.showmessage.set(false);
		 },
		 initRoomDimensions: function(){
		 	var app = this;
		 	$('[data-room]').change(function(){
		 		var value = $(this).data('room');
		 		// Save the changed value to the object
		 		app[value + 'Room'] = parseInt($(this).val()) || 0;
		 		app.setCookies();
		 		// Trigger all functions that are dependent on those values
		 		app.renderPageAfterRoomDimensions();
		 	});
		 },
		 renderPageAfterRoomDimensions: function(){
		 	if(this.heightRoom > 0){
		 		$('[data-room="height"]').val(this.heightRoom);
		 	}
		 	if(this.lengthRoom > 0){
		 		$('[data-room="length"]').val(this.lengthRoom);
		 	}
		 	if(this.widthRoom > 0){
		 		$('[data-room="width"]').val(this.widthRoom);
		 	}
		 	if(this.widthRoom > 0 && this.lengthRoom > 0){
		 		this.installKitQty.val(Math.ceil((this.widthRoom * this.lengthRoom) / 5));
		 		$('.install-kit-qty-message').removeClass('hidden');
		 	}
		 	else {
		 		this.installKitQty.val(1);
		 		$('.install-kit-qty-message').addClass('hidden');
		 	}
		 	if(typeof this.getSelectedProduct('floor') != "undefined" && typeof this.getSelectedProduct('wall') != "undefined"){
		 		this.outputBoardCalQuantities();
		 	}
		 	roomcalc.calculate();
		 },
		 /**
		  * Restore Session
		  * ---------------
		  * Shows the "would you like to restore your last sessions?" message
		  * and binds the click events to the 2 contained links
		  */
		  restoreSession: function(){
		  	var app = this;
		  	if(this.doCookiesExist()){
		  		$('#session-alert').addClass('active');
		  		$('[data-session="old"]').bind('touch click tap',function(){
		  			app.getCookies();
		  			app.renderPageAfterCookieLoad();
		  			$('#session-alert').removeClass('active');
		  		});
		  		$('[data-session="new"]').bind('touch click tap',function(){
		  			app.unsetCookies();
		  			$('#session-alert').removeClass('active');
		  		});
		  	}
		  },
		  /**
		   * Initialise Cookies
		   * ------------------
		   * For each cookie in the cookies array, create a new cookieClass object
		   * and save it in the cookiesObjs array.
		   */
		   initCookies: function(){
		   	var app = this;
		   	$(this.cookiesArray).each(function(index,entry){
		   		app.cookiesObjs[entry.id] = new cookieClass(entry.key,entry.value);
		   	});
		   },
		/**
		 * Get Handles
		 * -----------
		 * Initialise commonly used selectors
		 */
		 getHandles: function(){
		 	var app = this;
		 	this.steps = $('#mywetroom-steps');
		 	this.kitSelectionBlock = $('#mywetroom-kit-items');
		 	this.kitDisplay = $('#mywetroom-kit-picture');
		 	this.boardcal = $('#board-calculator');
		 	this.addToBasketButton = $('#add-to-basket');
		 	this.tooltip = $('#tooltip-error');
		 	this.loader = $('.loading-overlay');
		 	this.continueButton = $('.continue-button');
		 	this.installKitQty = $('.install-kit-qty');

			// Compile handles to all dt elements
			this.stepTitle = {};
			$('dt',this.steps).each(function(index, dt){
				app.stepTitle[$(dt).data('step')] = $(dt);
			});

			// Compile handles to all dd elements
			this.stepBody = {};
			$('dd',this.steps).each(function(index, dd){
				app.stepBody[$(dd).data('step')] = $(dd);
			});
		},
		/**
		  * Set Display Height
		  * ------------------------
		  * Take the width of the wetroom display and work out the height based
		  * on the ratio.
		  */
		  setDisplayHeight: function(){
		  	if(typeof this.kitDisplay != "undefined"){
		  		var width = this.kitDisplay.width();
		  		var ratio = 0.6;
		  		this.kitDisplay.css('height', width * ratio);
		  	}
		  },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /////////////////////////////////////////////////////////// DATA ///////////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 /**
		  * Initialise Prices
		  * -----------------
		  * Initialise all the static prices on the page and, if the price relates
		  * to a purchasable item, set the details into a selected products variable.
		  */
		  initPrices: function(){
		  	var app = this;
		  	$('.price[data-sku]').each(function(index,elem){
		  		app.getAjax({
		  			data: 'sku=' + $(elem).data('sku')
		  		}).done(function(xhr){
		  			var rsp = $.parseJSON(xhr);
		  			$(elem).html('£' + rsp.price);
		  			// If the price element has a parent step we also need to activate
		  			// its button and saved the product details into the selected products
		  			var step = app.getStepFromChild($(elem),true);
		  			if(step){
		  				app.activateButton($(elem));
		  				app.setSelectedProduct(step,rsp)['static'] = true;
		  			}
		  		})
		  	});
		  },
		 /**
		   * Get Parent Step
		   * ---------------
		   * Return the step value of the parent step, e.g. the DD.
		   *
		   * @param	jQuery	$output	The price output block under the parent step
		   * @return	String	The value of the parent step's step
		   */
		   getParentStep: function($output){
		   	return typeof $output.data('parent') == "undefined" ? $($output).closest('[data-parent]').data('step') : $output.data('step');
		   },
		/**
		 * Get Step From Child
		 * -------------------
		 * Returns a handle or string identifier for step given child element
		 *
		 * @param 	$child 	jQuery object of child element found in step
		 * @param   ref 	Boolean to determine whether we want a string
		 *                  rather than a jQuery object returned
		 * @return  mixed 	Either a string identifier or a jQuery object
		 */
		 getStepFromChild: function($child, ref){
		 	$step = typeof $child.data('step') == "undefined" ? $child.closest('[data-step]') : $child;
		 	if(typeof ref == "undefined"){
		 		return $step;
		 	}
		 	else {
		 		return $step.data('step');
		 	}
		 },
		/**
		 * Get Price Block
		 * ---------------
		 * Given any selectable option, this function returns the price block
		 * associated to the element
		 *
		 * @param 	$elem 	A jQuery object representing a DOM element
		 * @return 	jQuery 	A jQuery object representing the price block DOM
		 *                  element associated with the passed in DOM element
		 */
		 getPriceOutputBlock: function($elem){
		 	if($elem.find('.price-output-block').length > 0){
		 		return $elem.find('.price-output-block');
		 	}
		 	return $elem.closest('.price-output-block');
		 },
		/**
		 * Show Loader
		 * -----------
		 * Shows the loading animation inside the passed element
		 *
		 * @param  jQuery $elem jQuery object containing a loader animation
		 */
		 showLoader: function(message){
		 	if(typeof message != "undefined") {
		 		$('.loading-message', this.loader).html(message)
		 	}
		 	this.loader.addClass('active');
		 	$('body').addClass('modal-open');
		 },
		/**
		 * Hide Loader
		 * -----------
		 * Shows the loading animation inside the passed element
		 *
		 * @param  jQuery $elem jQuery object containing a loader animation
		 */
		 hideLoader: function(){
		 	this.loader.removeClass('active');
		 	$('body').removeClass('modal-open');
		 },
		/**
		 * Initialise Popup
		 * ----------------
		 * Setup the popup open and close buttons, along with the smooth scrolling
		 * effects and auto height setting.
		 */
		 initPopups: function(){
		 	var app = this;
			// Bind showing the specific schematics to the button with the same index
			$('[data-index]','.popup-controls').bind('click',function(){
				var index = $(this).data('index');
				var controls = $(this).closest('.popup-controls');
				var schematics = controls.siblings('.schematics');
				controls.find('[data-index]').removeClass('active');
				$(this).addClass('active');
				schematics.children().removeClass('active');
				schematics.children('[data-index="' + index + '"]').addClass('active');
			});
			// Bind showing the specific schematics to the button with the same
			// index for the mobile
			$('[data-index]','.slider-controls').bind('click',function(){
				var index = $(this).data('index');
				var controls = $(this).closest('.slider-controls');
				var schematics = controls.siblings('.slider-images');
				controls.find('[data-index]').removeClass('active');
				$(this).addClass('active');
				schematics.children('div').addClass('hidden');
				schematics.children('[data-index="' + index + '"]').removeClass('hidden');
			})
		 	// Bind the open/close event to the popups button
		 	$('[data-click]').bind('click',function(e){
		 		if($(this).hasClass('opened')){
		 			app.hideModal();
		 		}
		 		else {
		 			var id = $(this).data('click');
		 			var popup = $('[data-popup="' + id + '"]');
		 			app.showModal(id);
		 			// to handle slider on mobiles
		 			app.setStepHeight(popup);
		 			app.scrollScreenToPosition(popup.offset().top - 15);
		 		}
		 		app.resetHeights(500);
		 		e.stopImmediatePropagation();
		 	});
		 	$('[data-close="popup"]').bind('click',function(){
		 		app.hideModal();

		 		// Scroll the screen to the button the user clicked to open the popup
		 		var parentRef = $(this).closest('[data-popup]').data('popup');
		 		var parentPosition = $('[data-click="' + parentRef + '"]').offset().top - 15;
		 		app.scrollScreenToPosition(parentPosition);
		 	})
		 },
		 /**
		  * Show Modal
		  * ----------
		  *
		  * @param  String	id	The id of the popup to activate.
		  */
		  showModal: function(id){
		  	this.hideModal();
		  	$('[data-click="' + id + '"]').addClass('opened');
		  	$('body').addClass('modal-open');
		  	$('.modal-popup').addClass('active');
		  	$('[data-popup="' + id + '"]').addClass('active');
		  },
		 /**
		  * Hide Modal
		  * ----------
		  */
		  hideModal: function(){
		  	var app = this;
		  	$('[data-click]').removeClass('opened');
		  	$('body').removeClass('modal-open');
		  	$('.modal-popup').removeClass('active');
		  	$('[data-popup]').removeClass('active');
		  	$('[data-popup]').each(function(){
		  		$(this).css('max-height','0px');
		  	});
		  },
		/**
		 * Get Ajax
		 * --------
		 * Returns a handle to a AJAX object
		 *
		 * @params Object	An object contianing the ajax options
		 * @return jQuery	ajax object
		 */
		 getAjax: function(ajaxOptions){
		 	var options = {
		 		method: 'post',
		 		url: this.url.baseUrl + this.url.productLookup
		 	};
		 	options = $.extend(options,ajaxOptions);
		 	this.ajaxObj = $.ajax(options);
		 	this.ajaxObj.fail(function(xhr){
		 		if(xhr.status == 404){
		 			console.log("404 - The page you've requested doesn't exist.");
		 		}
		 		else {
		 			console.log('ERROR: ' + xhr.responseText);
		 		}
		 	});
		 	return this.ajaxObj;
		 },
		/**
		 * Set price
		 * ---------
		 * Set the price-block price value
		 *
		 * @param jQuery 	$output A handle to the dom element that contains the price
		 * @param float 	price 	The value we want to output
		 */
		 setPrice: function($output, price){
		 	$('.price', $output).html('£' + parseFloat(price).toFixed(2));
		 },
		/**
		 * Get Price
		 * ---------
		 * Gets the price via ajax
		 *
		 * @param	string 		sku			The sku we want to lookup
		 * @param	function	callback	A function to be executed on success
		 */
		 getPrice: function(sku,callback){
		 	var app = this;
		 	return this.getAjax({
		 		data: 'sku=' + sku
		 	}).done(function(data){
		 		var parsedJsonData = JSON.parse(data);
		 		if(typeof callback != 'undefined'){
		 			callback(parsedJsonData);
		 			app.setCookies();
		 		}
		 	});
		 },
		 /**
		  * Reset Incompatibilities
		  * -----------------------
		  * Reset the state of anything with a data-type attribute and then
		  * iterate over the selected items object, disabling any incompatible
		  * product options.
		  */
		  resetIncompatibles: function(){
		  	$('[data-type]').removeClass('disabled')
		  	.removeProp('disabled')
		  	.removeAttr('disabled');
		  	// Reset the incompatibilities by looping through the selectedItems
		  	// array and adding the disabled class to any of the incompatibilities.
		  	for(var key in this.selectedItems){
		  		var item = this.selectedItems[key];
		  		var itemObj = this.errorLookupObject[item]
		  		for(var type in itemObj) {
		  			var typeObject = itemObj[type];
		  			for(var ref in typeObject) {
		  				$('[data-value="' + ref + '"]').addClass('disabled')
		  				.prop('disabled','disabled')
		  				.attr('disabled','disabled');
		  			};
		  		};
		  	};
		  },
		   /**
		  * Is Number Valid
		  * ---------------
		  * Check to see whether a number only contains digits and dots.
		  *
		  * @param Int	num	The number to be checked.
		  * @return	Boolean	Whether the number is valid or not.
		  */
		  isNumberValid: function(num){
		  	if(/^[\d\.]*$/.test(num) && num.length > 0){
		  		return true;
		  	}
		  	return false;
		  },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //////////////////////////////////////////////////////// NAVIGATION ////////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Initialise Accordion
		 * --------------------
		 * Initialise the wetroom step's elements, binding the click events, set
		 * heights and smooth scrolling to the titles and step links.
		 */
		 initAccordion: function(){
		 	var app = this;
		 	$('dt, [data-step-link]',this.steps).bind('click',function(){
		 		var elem = this;
		 		var step = typeof $(elem).data('step-link') == 'undefined' ? $(elem).data('step') : $(elem).data('step-link');
		 		if(typeof $(elem).data('step-link') != 'undefined'){
		 			app.unlockStep($(elem).data('step-link'));
		 		}
		 		if($(app.stepTitle[step]).hasClass('locked')){
		 			return false;
		 		}
		 		app.continueButton.removeClass('hidden');
		 		app.setStepHeight($(app.stepBody[step]));
		 		app.toggleStep($(app.stepTitle[step]));
		 		// Set position of open accordion and scroll screen so its at the top
		 		var position = $(app.stepTitle[step]).offset().top - 15;
		 		app.scrollScreenToPosition(position, 500, 'easeInOutQuint');
		 	});
		 },
		 /**
		  * Get Active Skip
		  * ---------------
		  * Find any data skip attributes that might exist on selected options.
		  * @param  {[type]} stepName [description]
		  * @return {[type]}          [description]
		  */
		  getActiveSkip: function($step){
		  	return $step.find('.option.selected[data-skip]').data('skip');
		  },
		/**
		 * Toggle Step
		 * -------------
		 * Opens/closes a main accordion leaf
		 *
		 * @param  jQuery $step The jQuery selector of the step we want to toggle
		 */
		 toggleStep: function($step){
		 	$step.toggleClass('active').siblings('dt').removeClass('active');
		 },
		 /**
		  * Scroll To Element
		  * -----------------
		  * Animate a smooth scroll to the given element.
		  *
		  * @param  jQuery	$elem	The given element.
		  */
		  scrollScreenToPosition: function(position, duration, animation, onComplete){

		  	var position = position || 0;
		  	var duration = duration || 500;
		  	var animation = animation || 'easeInOutCubic';
		  	var onComplete = onComplete || function(){};

		  	$('html,body').animate(
		  		{scrollTop: position},
		  		duration,
		  		animation,
		  		onComplete
		  		);
		  },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //////////////////////////////////////////////////////// SELECTIONS ////////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Initialise Options
		 * ------------------
		 * Initialise the options, binding the set relevant option controller and
		 * set height to the different option elements.
		 */
		 initOptions: function(){
		 	var app = this;
		 	$('.option[data-type]').bind('click',function(){
		 		app.setOptionStatus($(this));
		 		if($(this).siblings('.warning').length > 0){
		 			app.setStepHeight(app.getStepFromChild($(this)));
		 			app.resetHeights(500);
		 		}
		 	});
		 	$('select.selectable').bind('change',function(){
		 		app.setOptionStatus($(this).children('option:selected'));
		 	});
		 	$('input[type="radio"],select','[data-mat="mat"]').change(function(){
		 		app.matSelection($(this));
		 	});
		 },
		/**
		 * Initialise Mat Product
		 * ----------------------
		 * Bind to the mat product inputs the function that handles the active
		 * stat box.
		 */
		 initMatProduct: function(){
		 	$('.stats input[type="radio"]').bind('change',function(){
		 		$('.stats li').removeClass('active');
		 		$(this).closest('li').addClass('active');
		 	});
		 },
		/**
		 * Set Active Group Item
		 * ---------------------
		 * Sets the 'selected' class on the passed element and removes the same
		 * class from all siblings in group
		 *
		 * @param jQuery	$elem	The clicked element
		 */
		 setActiveGroupItem: function($elem){
		 	var $group = $('[data-type="' + $elem.data('type') +'"]');
		 	if($elem.hasClass('selected')){
		 		$group.removeClass('selected');
		 	}
		 	else {
		 		$group.removeClass('selected');
		 		$elem.addClass('selected');
		 	}
		 },
		 /**
		  * Get Selected Product
		  * --------------------
		  * Access the selected product array and return a handle to a given entry.
		  *
		  * @param String	step	The name of the step being queried.
		  * @return	Obj	a handle to the given step's selected product.
		  */
		  getSelectedProduct: function(step){
		  	return this.selectedProducts[step];
		  },
		 /**
		  * Set Selected Product
		  * --------------------
		  * Initally set an entry in the selected products array and return a handle
		  * to that particular entry for future chaining.
		  *
		  * @param	String	step	The name of the step being set against.
		  * @param	Mixed	value	Any value to be initially set against that step.
		  * @return	Obj	A handle to the given step's selected product.
		  */
		  setSelectedProduct: function(step,value){
		  	this.selectedProducts[step] = value;
		  	return this.selectedProducts[step];
		  },
		 /**
		  * Unset Selected Product
		  * ----------------------
		  * Remove a step's selected product entry form the selected products array
		  */
		  unsetSelectedProduct: function(step){
		  	delete this.selectedProducts[step];
		  },
		/**
		 * Set Option Status (controller)
		 * ------------------------------
		 * Gets applied to all selectable options and is responsible for setting
		 * the active state, the group status for siblings and the values in
		 * the master JS selectedItems object.
		 *
		 * @param jQuery	$this	A handle to the selection that's been
		 * interacted with.
		 */
		 setOptionStatus: function($elem){
		 	if($elem.hasClass('disabled') || $elem.hasClass('unclickable')){
		 		return false;
		 	}
		 	if(typeof $elem.data('value') == "undefined"){
		 		console.log('No data-value on ',$elem);
		 		return false;
		 	}
		 	// Get handles and values
		 	var app = this;
		 	var value = $elem.data('value');
		 	var group = $elem.data('type');
		 	var $step = this.getStepFromChild($elem);
		 	var stepName = this.getStepFromChild($elem,true);
		 	var $output = this.getPriceOutputBlock($('[data-step="' + stepName + '"]'));

		 	// Callback function is the function to be executed once the AJAX has returned
		 	var callback = function(prodJson){
		 		if(typeof prodJson.error != 'undefined'){
		 			app.unsetProductSelection(stepName);
		 			console.log('ERROR: ' + prodJson.error);
		 			return false;
		 		}
		 		// add the returned product object to the main selection object
		 		app.setSelectedProduct(stepName,prodJson);
		 		app.setPrice($output,prodJson.price);

		 		if(app.isNumberValid(app.getQuantity($output)) && typeof app.getSelectedProduct(stepName) != "undefined"){
		 			app.activateButton($output);
		 		}
		 		else {
		 			app.deactivateButton($output);
		 		}

		 		// Create function to add pulse animation class to button once page scroll is complete
		 		var buttonFunction = function(){
		 			var $button = $(this);
		 			$button.addClass('grab-attention');
		 			setTimeout(function(){
		 				$button.removeClass('grab-attention');
		 			},2000);
		 		};
		 		// Scroll screen to show price block at bottom of device
		 		var position = $output.offset().top + $output.outerHeight(true) - $(window).height() + 40;
		 		app.scrollScreenToPosition(position, 500, 'easeInOutCubic', buttonFunction.bind($('button', $output)));

		 		app.setProductSelection(stepName,prodJson);
		 	}

		 	// Update the selection on screen, get the price, update the JS selection object
		 	// Right, we're ready to go, make the screen show the selections
		 	if($elem.hasClass('selected')){
		 		this.deleteSelectedItem(group);
		 		this.unsetSelectedProduct(stepName);
		 		this.unsetProductSelection(stepName);
		 	}
		 	else {
		 		this.setSelectedItem(group,value);
		 		var sku = $elem.data('sku') || false;
		 		if(sku == false){
		 			var sku = this.getGroupSku($elem.data('group'));
		 		}
		 		if(sku != false){
		 			// Make all the sibling options unclickable until the AJAX call returns
		 			$elem.closest('.selectable').find('.option').not($elem).addClass('unclickable');
		 			this.showLoader();
		 			// Uses callback defined above
		 			this.getPrice(sku,callback)
		 			.always(function(){
		 				app.hideLoader();
		 				// Make the sibling options clickable again
		 				$('.unclickable').removeClass('unclickable');
		 			});
		 		}
		 	}
		 	this.setCookies();
		 	this.setActiveGroupItem($elem);
		 	this.resetIncompatibles();
		 },
		 /**
		  * Get Group SKU
		  * -------------
		  * Generate a SKU based on the selections of a given element's group.
		  *
		  * @param	String	group	The given element's group.
		  * @return	Mixed	A string relating to the group's SKU or false if all
		  * the selections of the group haven't been amde.
		  */
		  getGroupSku: function(group){
		  	var app = this;
		  	var types = [];
		  	var sku = '';
		  	$('[data-group="' + group + '"]').each(function(index, type){
		  		if(types.indexOf($(type).data('type')) < 0){
		  			types.push($(type).data('type'));
		  		}
		  	});

		  	$(types.sort()).each(function(index,value){
		  		if(!(value in app.selectedItems)){
		  			sku = '';
		  			return false;
		  		}
		  		else {
		  			sku += app.selectedItems[value] + '|';
		  		}
		  	})
		  	return sku == '' ? false : sku.substring(0, sku.length - 1);
		  },
		/**
		 * Set Selected Item
		 * -----------------
		 * Sets a value into the selectedItems JS object.
		 *
		 * @param string 	key 	The object key
		 * @param string 	value 	The object value
		 */
		 setSelectedItem: function(key, value){
		 	this.selectedItems[key] = value;
		 },
		/**
		 * Delete Selected Item
		 * --------------------
		 * Deletes an entry from the selectedItems JavaScript Object
		 *
		 * @param	String	key	The key of the entry to be removed
		 *
		 */
		 deleteSelectedItem: function(key){
		 	delete this.selectedItems[key];
		 },
		 /**
		  * Mat Selection
		  * -------------
		  * The event to be trigger when any of the mat bundle options are selected.
		  * Take the length and stat option and return a product object relating
		  * to the bundle product.
		  *
		  * @param	jQuery	$triggerElem	The element that triggered the mat
		  * selection function being fired.
		  */
		  matSelection: function($triggerElem){
		  	var app = this;
		  	var $step = this.getStepFromChild($triggerElem);
		  	var stepName = this.getStepFromChild($triggerElem,true);
		  	var $output = this.getPriceOutputBlock($step);
		  	var length = $('option:selected',$step).val();
		  	var stat = $('input[type="radio"]:checked',$step).data('sku');

		  	if(stat != null && length != null){
		  		this.showLoader();
		  		app.getAjax({
		  			url: app.url.baseUrl + app.url.matLookup,
		  			data: {
		  				stat: stat,
		  				length: length
		  			}
		  		}).done(function(data){
		  			var prodJson = JSON.parse(data);
		  			if(typeof prodJson.error == 'undefined'){
		  				// Add the returned product object to the main selection object
		  				app.setSelectedProduct(stepName,prodJson);

		  				if(app.isNumberValid(app.getQuantity($output)) && typeof app.getSelectedProduct(stepName) != "undefined"){
		  					app.activateButton($output);
		  				}
		  				else {
		  					app.deactivateButton($output);
		  				}

		  				app.setProductSelection(stepName,prodJson);
		  			}
		  		}).always(function(){
		  			app.hideLoader();
		  		});
		  	}
		  },
		 /**
		  * Set Product Selection
		  * ---------------------
		  * Sets the product name into the step selection, activates the button
		  * and outputs the price.
		  *
		  * @param  String	step	The step to be set.
		  * @param  Object	obj		The product object that needs to be output.
		  */
		  setProductSelection: function(step,productObj){
		  	var $step = $('[data-step="' + step + '"]');
		  	var stepName = this.getParentStep($step);
		  	var $output = this.getPriceOutputBlock($step);
		  	var $title = this.stepTitle[stepName];

		  	$('.product',$title).html(productObj.name);
		  	this.setPrice($output,productObj.price);
		  	$('.selection', $title).addClass('active');
		  },
		  /**
		   * Unset Product Selection
		   * -----------------------
		   * Unsets the product name into the step selection, deactivates the
		   * button and reverts the price to zero.
		   *
		   * @param	String	step	The step to be unset.
		   */
		   unsetProductSelection: function(step){
		   	var app = this;
		   	var $output = this.getPriceOutputBlock($('[data-step="' + step + '"]'));
		   	var step = app.getParentStep($output);
		   	var $title = this.stepTitle[step];

		   	$('.product',$title).html('');
		   	$('.selection', $title).removeClass('active');
		   	app.setPrice($output,0);
		   	this.deactivateButton($output);
		   },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ///////////////////////////////////////////////////////// TOOLTIP //////////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Initialise Tooltip
		 * ------------------
		 * Bind to the disabled option's mouseover, mousemove and mouseout event
		 * the tooltip activation / deactivation and reposition.
		 */
		 initTooltip: function(){
		 	var app = this;
		 	$('ul.selectable').on({
		 		"mouseover": function(){
		 			var errorMessage = app.getErrorCode($(this));
		 			if(errorMessage){
		 				app.tooltip.addClass('active');
		 				app.outputError(errorMessage);
		 			}
		 		},
		 		"mousemove": function(e){
		 			app.repositionTooltip(e);
		 		},
		 		"mouseout": function(){
		 			app.tooltip.removeClass('active');
		 		}
		 	},'.option.disabled');
		 },
		 /**
		  * Reposition Tooltip
		  * ------------------
		  * Reposition the tooltip to the mouse pointer, taking into consideration
		  * window width, element width / height, and the placement near the edges
		  * of the screen.
		  *
		  * @param  Event	event	A handle to the trigger event.
		  */
		  repositionTooltip: function(event){
		  	var topPos = event.pageY - $('body').scrollTop();
		  	var leftPos = event.pageX;
		  	var arrow = this.tooltip.children('.arrow');
		  	if(topPos > this.tooltip.outerHeight()){
		  		topPos = topPos - this.tooltip.outerHeight();
		  		arrow.removeClass('bottom').addClass('top');
		  		topPos -= 20;
		  	}
		  	else {
		  		arrow.removeClass('top').addClass('bottom');
		  		topPos += 20;
		  	}
		  	if(leftPos > ($(window).outerWidth() - this.tooltip.outerWidth())){
		  		leftPos = leftPos - this.tooltip.outerWidth();
		  		arrow.removeClass('left').addClass('right');
		  		leftPos += 137;
		  	}
		  	else {
		  		arrow.removeClass('right').addClass('left');
		  		leftPos -= 35;
		  	}
		  	this.tooltip.css('top',topPos).css('left',leftPos);
		  },
		 /**
		  * Get Error Code
		  * --------------
		  * Gets the error code of the given element, based on the incompatibilities
		  * of the currently selected items.
		  *
		  * @param  jQuery	$elem	The given element.
		  * @return String	The error code.
		  */
		  getErrorCode: function($elem){
		  	var app = this;
		  	// Grab an identifier from the passed in dom element
		  	var value = $elem.data('value');
	  		// Use the identifier from the dom element to find the right groups in the errorLookupObject
	  		var errorTypes = app.errorLookupObject[value];

	  		for(var selectedType in this.selectedItems){
	  			var item = app.selectedItems[selectedType];
	  			var typeObject = errorTypes[selectedType];
	  			if(typeof typeObject != "undefined") {
	  				var option = typeObject[item];
	  				if(typeof option != "undefined") {
	  					return {
	  						'title': this.errorTitles[selectedType],
	  						'message': this.errorMessages[selectedType].format(errorTypes.type, option)
	  					};
	  				}
	  			}
	  		}
	  	},
		 /**
		  * Output Error
		  * ------------
		  * Taking the element with an error code, output the error message to
		  * the tooltip's output.
		  *
		  * @param  string	errorMessage	The message we want to output
		  */
		  outputError: function(errorMessage){
		  	$('.title',this.tooltip).html(errorMessage.title);
		  	$('.message',this.tooltip).html(errorMessage.message);
		  },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ///////////////////////////////////////////////////// BOARD CALCULATOR /////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Output Main Total
		 * -----------------
		 * Take the price from the selected products for every product that has a checked box and multiple it by the quantity
		 */
		 outputBoardCalTotal: function(){
		 	var app = this;
		 	price = 0;
		 	if($('[data-step="floor"]').find('.required input[type="checkbox"]:checked').length > 0){
		 		price += $('.board-cal-floor').val() * this.getSelectedProduct('floor').price;
		 	}
		 	if($('[data-step="wall"]').find('.required input[type="checkbox"]:checked').length > 0){
		 		price += $('.board-cal-wall').val() * this.getSelectedProduct('wall').price;
		 	}
		 	var $output = this.getPriceOutputBlock(this.getStepFromChild(this.boardcal));
		 	this.setPrice($output,price);
		 	if(price <= 0){
		 		app.deactivateButton($output);
		 	}
		 	else {
		 		app.activateButton($output);
		 	}
		 },
		/**
		 * Board Calculator Calculation
		 * ----------------------------
		 * Take the width, length and height and work out the quantities. After that trigger the main price calculation
		 */
		 outputBoardCalQuantities: function(){
		 	var width = $('.width', this.boardcal).val();
		 	var height = $('.height', this.boardcal).val();
		 	var length = $('.length', this.boardcal).val();
		 	var area = $('input[name="area"]:checked', this.boardcal).val();

		 	if(width != '' && height != '' && length != ''){
		 		var floor = width * length;
		 		var wall = (height * length) + (height * width);
		 		if(area == 'whole'){
		 			wall = wall * 2;
		 		}
		 		$('.board-cal-floor').val(this.getBoardCalTotal(floor));
		 		$('.board-cal-wall').val(this.getBoardCalTotal(wall));
		 		this.outputBoardCalPrices($('[data-step="floor"]'));
		 		this.outputBoardCalPrices($('[data-step="wall"]'));
		 	}
		 },
		 /**
		  * Board Calculator Equation
		  * -----------------------
		  * The actual equation behind the Board Calculator
		  *
		  * @param  Int	value	The amount of meterage.
		  * @return Int	The meterage after coverage and wastage allowances.
		  */
		  getBoardCalTotal: function (value){
		  	var coverage = 0.72;
		  	var wastage = 0.95;
		  	return Math.ceil((value / coverage) * wastage);
		  },
		/**
		 * Bind Events To Inputs
		 * ---------------------
		 * Bind the appropriate events to their relevant inputs
		 */
		 initBoardCal: function(){
		 	var app = this;
		 	$('input',this.boardcal).bind('keyup change',function(){
		 		app.outputBoardCalQuantities();
		 	});
		 	$('input[type="checkbox"]','#board-totals').bind('keyup change',function(){
		 		app.outputBoardCalTotal();
		 	});
		 	$('input[name="qty"]','#board-totals').bind('keyup change',function(){
		 		app.outputBoardCalPrices(app.getStepFromChild($(this)));
		 	});
		 },
		/**
		 * Output Individual Prices
		 * ------------------------
		 * Take the selected product price and multiple it by the quantity
		 */
		 outputBoardCalPrices: function($step){
		 	var stepName = $step.data('step');
		 	var price = this.getSelectedProduct(stepName).price;
		 	var qty = $('.board-cal-' + stepName).val();
		 	$step.find('[data-product="price"]').html((price * qty).toFixed(2)).parent().removeClass('hidden');
		 	this.outputBoardCalTotal();
		 },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /////////////////////////////////////////////////////// KIT / BASKET ///////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		 /**
		  * Add Step To Kit
		  * ---------------
		  * This is a centralised function for handling the adding of a step to the kit.
		  * It handles ensuring the quantity is valid, adding the product to the kit,
		  * outputting the images, outputting the product sleection for static products,
		  * saving the current progress to cookies and the post-add animations.
		  *
		  * @param	jQuery	$step	A handle to the step tha tneeds adding to the kit.
		  */
		  addStepToKit: function($step){
		  	var app = this;
		  	var stepName = $step.data('step');
		  	var imageClass = app.stepImageMapping[stepName];
			// Confirm the quantity is valid
			if(!this.isNumberValid(this.getQuantity($step))){
				return false;
			}
			// Add the product to the kit
			this.addProductToKit(stepName);
			// If the product is a static price, set the product selection
			if("static" in this.getSelectedProduct(stepName)){
				this.setProductSelection(stepName,this.getSelectedProduct(stepName));
			}
			// Output selected product to the wetroom kit illustration
			this.getAjax({
				url: app.url.baseUrl + app.url.imageLookup,
				data: 'sku=' + app.getSelectedProduct(stepName)['sku']
			}).done(function(data){
				var parsedJsonData = JSON.parse(data);
				if(typeof parsedJsonData.error != 'undefined' || parsedJsonData.length == 0){
					app.outputImage(imageClass,imageClass);
				}
				else {
					for(var imgClass in parsedJsonData){
						app.outputImage(parsedJsonData[imgClass],imgClass);
					}
				}
			});
			// Save the progress of the journey so far
			this.setCookies();
			var afterAnim = function(){
				// After the product has been added, close the current accordion
				// leaf and mark the step as completed
				var parentStep = app.getParentStep($step);
				app.toggleStep(app.stepTitle[parentStep]);
				app.completedSteps[parentStep] = parentStep;
				app.stepTitle[parentStep].addClass('completed');
				// If we have any active data-skips then action accordingly otherwise
				var skip = app.getActiveSkip($step);
				if(skip){
					$(app.stepUnlockMap[parentStep]).each(function(index,value){
						app.unlockStep(value);
					});
					app.unlockStep(skip);
					app.setStepHeight($(app.stepBody[skip]));
					app.toggleStep($(app.stepTitle[skip]));
				}
				// Otherwise unlock the next step so the user can continue their journey
				else {
					if(parentStep in app.stepUnlockMap){
						$(app.stepUnlockMap[parentStep]).each(function(index,value){
							app.unlockStep(value);
							if(index == 0){
								app.setStepHeight($(app.stepBody[value]));
								app.toggleStep($(app.stepTitle[value]));
							}
						})
					}
				}
			};
			// If we are at the end of our journey, hide the continue button
			if($step.hasClass('no-continue')){
				app.continueButton.addClass('hidden');
			}
			// If the step has any product dependencies action them now
			if(typeof $step.data('dependent') != "undefined"){
				this.resolveDependencies($step);
			}

			var position = app.kitSelectionBlock.offset().top - 15;
			app.scrollScreenToPosition(position, 750, 'easeInOutCubic', afterAnim.bind($(this)));

		},
		/**
		 * Resolve Dependencies
		 * --------------------
		 * If we have any products that depend on choices made in a certain step,
		 * we resolve them by retrieving the product details from the server, saving
		 * them to the selected products object, then outputting any particular value
		 * in the dependencies element, allowing for any regular expressions.
		 *
		 * @param	jQuery	$step	A handle to the step with dependents
		 */
		 resolveDependencies: function($step){
		 	var app = this;
		 	var sku = $step.find('.option.selected[data-dependency]').data('dependency') || '';
			// The callback function handles the setting of product data to the JavaScript
			// object and outputting values to the dependents elements.
			var callback = function(prodJson){
				var $productStep = $('[data-step="' + $step.data('dependent') + '"]');
				app.setSelectedProduct($step.data('dependent'),prodJson)
				for(key in prodJson){
					var $entry = $productStep.find('[data-product="' + key + '"]');
					var value = prodJson[key];

					if($entry.data('regex')){
						var regex = new RegExp($entry.data('regex'),"g");
						value = regex.exec(value)[0];
					}
					$entry.html(value);
				}
			}
			this.getPrice(sku,callback);
		},
		/**
		 * Initialise Add To Kit
		 * ---------------------
		 * Initialise the add to kit buttons, binding the add to kit function,
		 * output the selection for static prices and output the images on the
		 * wetroom kit illustration.
		 */
		 initAddToKits: function(){
		 	var app = this;
		 	$(this.steps).on('click','.price-output-block button.active',function(){
		 		var $parent = app.stepBody[app.getParentStep($(this))];
		 		if($(this).hasClass('multi-product')){
		 			$parent.find('[data-step]').each(function(index,elem){
		 				if($(elem).find('.required input[type="checkbox"]:checked').length > 0){
		 					app.addStepToKit($(elem));
		 				}
		 			});
		 		}
		 		else {
		 			app.addStepToKit($parent);
		 		}

		 	});
		 },
		 /**
		  * Unlock Step
		  * -----------
		  * Unlock a given accordion title and save a list of the unlocked steps.
		  *
		  * @param	String	step	The accordion title to be actioned
		  */
		  unlockStep: function(step){
		  	this.stepTitle[step].removeClass('locked');
		  	this.unlockedSteps[step] = step;
		  },
		 /**
		  * Add To Basket
		  * -------------
		  * Take the kit object and add it to the Magento basket.
		  */
		  addToBasket: function(){
		  	var app = this;
		  	var output = this.kitSelectionBlock.find('.price-output-block');
		  	if(this.addToBasketButton.hasClass('active')){
		  		this.showLoader('Adding your items to the basket');
		  		this.deactivateButton(output);

		  		this.getAjax({
		  			url: app.url.baseUrl + app.url.addBasket,
		  			data: {
		  				data: JSON.stringify(this.kitItems),
		  				total: app.kitTotal
		  			}
		  		}).done(function(){
		  			app.unsetCookies();
		  			location.reload();
		  		}).always(function(xhr){
		  			app.hideLoader();
		  			app.activateButton(output);
		  		})
		  	}
		  },
		  /**
		  * Add To Kit
		  * ----------
		  * With a handle to the given step, take the selected products product data and insert
		  * it into the kit obj, retrieving the quantity from the QTY input and
		  * appending to the kit if the product already exists.
		  *
		  * @param String	step	A reference to the step.
		  */
		  addProductToKit: function(step){
		  	var productObj = this.getSelectedProduct(step);
		  	var qty = this.getQuantity($('[data-step="' + step + '"]'));
		  	var foundItem = {};
		  	// Return false if the qty passed is zero or less
		  	if(qty <= 0){
		  		return false;
		  	}
		  	// Iterate over the kit items and see if the product already exists.
		  	$(this.kitItems).each(function(index,item){
		  		if(item.sku == productObj.sku){
		  			foundItem = item;
		  		}
		  	});
		  	// If the product already existes, increase its quantity
		  	if("sku" in foundItem){
		  		var index = this.kitItems.indexOf(foundItem);
		  		var itemQty = String(parseInt(foundItem.qty) + parseInt(qty));
		  		this.updateQty(index,itemQty);
		  	}
		  	// Otherwise add a new product to the kit items
		  	else {
		  		productObj['qty'] = qty
		  		productObj['step'] = step;
		  		this.kitItems.push(productObj);
		  	}
		  	// If we updated the quantity of an item then we don't need to animate
		  	// the kit, unless we've added a new item, either way render the kit
		  	// items on screen and scroll it into vision
		  	if("sku" in foundItem){
		  		this.renderKitSelectionBlock();
		  	}
		  	else {
		  		this.renderKitSelectionBlock('add');
		  	}
		  },
		/**
		 * Get Quantity
		 * ------------
		 * Get the nearest price output block and return its quantity input's value.
		 * @param	jQuery	$elem	The element passed.
		 * @return	Int	The quantity of the nearest price output block.
		 */
		 getQuantity: function($elem){
		 	return this.getStepFromChild($elem).find('input[name="qty"]').val();
		 },
		/**
		 * Bind Input Validation
		 * ---------------------
		 * Bind to all number typed inputs a frontend validation to check against
		 * empty inputs, scientific notation and minus numbers.
		 */
		 bindInputValidation: function(){
		 	var app = this;
		 	$('.price-output-block input[type="number"]').on('keyup change',function(){
		 		$(this).removeClass('invalid');
		 		if(!app.isNumberValid($(this).val())){
		 			$(this).addClass('invalid');
		 		}
		 		if(app.isNumberValid(app.getQuantity($(this))) && typeof app.getSelectedProduct(app.getStepFromChild($(this),true)) != "undefined"){
		 			app.activateButton($(this));
		 		}
		 		else {
		 			app.deactivateButton($(this));
		 		}
		 	});
		 },
		 /**
		  * Initialise Continue Button
		  * --------------------------
		  * The continue button in the kit items block should scroll the page
		  * to the currently opened step leaf
		  */
		  initContinueButton: function() {
		  	var app = this;

		  	this.continueButton.click(function(){
		  		var position = $('dt.active', this.steps).offset().top - 15;
		  		app.scrollScreenToPosition(position);
		  	});
		  },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  //////////////////////////////////////////////////// YOUR KIT SELECTION ////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Initialise Kit Items
		 * --------------------
		 * Initialise the kit items block, binding the remove item, update quantity,
		 * reset selection and add to basket functions to their relevant buttons.
		 */
		 initKitItems: function(){
		 	var app = this;
		 	this.addToBasketButton.bind('click',function(){
		 		app.addToBasket();
		 	});
		 	$('#reset-selection').bind('click',function(){
		 		app.resetProductSelection();
		 	});
		 	$(this.kitSelectionBlock).on('keyup change','input',function(){
		 		var index = $(this).closest('tr').data('index');
		 		app.updateQty($(this).closest('tr').data('index'),$(this).val());
		 		app.renderKitSelectionBlock();
		 		// Refocus on the destroyed input
		 		app.kitSelectionBlock.find('tr[data-index="' + index + '"] input').val($(this).val()).focus();
		 	});
		 	$(this.kitSelectionBlock).on('click','.remove-button',function(){
		 		app.removeItem($(this).closest('tr').data('index'));
		 	});
		 },
		 /**
		  * Update Quantity
		  * ---------------
		  * Update the quantity of a JavaScript Kit Object item.
		  *
		  * @param  Int	index	The idnex of product in the kit.
		  * @param  Int	qty	The quantity to update the proudct to.
		  */
		  updateQty: function(index,qty){
		  	this.kitItems[index].qty = qty;
		  },
		 /**
		  * Remove Item
		  * -----------
		  * Given a particular index, remove an item from the JavaScript Kit Object.
		  *
		  * @param  Int	The kit index.
		  */
		  removeItem: function(index){
		  	var app = this;
		  	var step = this.kitItems[index].step;
		  	var imgClass = this.stepImageMapping[step];
		 	// If multiple imgs exists under the step
		 	if(typeof imgClass == 'object'){
		 		$(imgClass).each(function(index,elem){
		 			app.outputImage('default',elem);
		 		})
		 	}
		 	else {
		 		app.outputImage('default',imgClass);
		 	}
		 	delete app.completedSteps[step];
		 	app.stepTitle[step].removeClass('completed');
		 	this.kitItems.splice(index,1);
		 	this.renderKitSelectionBlock();
		 },
		 /**
		  * Render kit selection block
		  * --------------------------
		  * Destroys the existing DOM elements inside the kit selection block,
		  * then rebuilds it using the JS selected products
		  */
		  renderKitSelectionBlock: function(add){
		  	var app = this;
		  	var $table = $('table',this.kitSelectionBlock);
		  	$table.html('');
		  	$(this.kitItems).each(function(index,elem){
		  		if(elem.qty == 0){
		  			app.removeItem(index);
		  			return true;
		  		}
		  		//------------------------------------------------------------------//
		  		// Foreach kit item, determine the classes and append it to the kit //
		  		//------------------------------------------------------------------//
		  		var itemClass = (typeof add != "undefined" && index == app.kitItems.length - 1) ? '' : 'show';
		  		var inputClass = '';
		  		if(!app.isNumberValid(elem.qty)){
		  			inputClass = 'invalid';
		  		}
		  		var $row = $('<tr data-index="' + index + '" class="' + itemClass + '"></tr>');
		  		$row.append('<td class="item-name"><div>' + elem.name + '</div></td>');
		  		$row.append('<td class="qty"><div>qty:</div></td>');
		  		$row.append('<td class="qty-value"><div><input type="number" class="' + inputClass + '" name="' + elem.sku + '" value="' + elem.qty + '"/></div></td>');
		  		$row.append('<td class="item-price"><div>£' + elem.price + '</div></td>');
		  		$row.append('<td class="remove-item"><div class="remove-button"></div></td>');
		  		$table.append($row);
		  	});
	  		//-----------------------------------------------------//
	  		// If we are adding an item then we need to animate it //
	  		//-----------------------------------------------------//
	  		if (typeof add != "undefined"){
	  			setTimeout(function(){
	  				$('tbody', $table).children('tr').last().addClass('show');
	  			},10);
	  		}
	  		//----------------------------------//
	  		// Then total the kit and output it //
	  		//----------------------------------//
	  		app.kitTotal = 0.00;
	  		this.kitItems.map(function(item){
	  			app.kitTotal += parseFloat(item.price) * parseInt(item.qty);
	  		});
	  		$('.price',this.kitSelectionBlock).html("<div>£" + app.kitTotal.toFixed(2) + '</div>');
	  		app.showMessage = app.kitItems.length > 0 ? true : false;
	  		app.checkKitItemsOutput();
	  		app.setCookies();
	  	},
		 /**
		   * Check Kit Items Output
		   * ----------------------
		   * Deterine whether the kit items output block should be visible or not.
		   *
		   * @return	Boolean	Whether the basket is active.
		   */
		   checkKitItemsOutput: function(){
		   	var app = this;
		   	$kitItemOutput = this.getPriceOutputBlock(this.addToBasketButton);
		   	$kitItemError = this.kitSelectionBlock.find('.invalidation-error');
		   	if(this.kitItems.length <= 0){
		   		this.addToBasketButton.removeClass('active');
		   		$kitItemOutput.removeClass('hidden');
		   		$kitItemError.addClass('hidden');
		   		return false;
		   	}
		   	if(this.kitSelectionBlock.find('input[type="number"].invalid').length > 0){
		   		this.addToBasketButton.removeClass('active');
		   		$kitItemOutput.addClass('hidden');
		   		$kitItemError.removeClass('hidden');
		   		return false;
		   	}
		   	this.addToBasketButton.addClass('active');
		   	$kitItemOutput.removeClass('hidden');
		   	$kitItemError.addClass('hidden');
		   	return true;
		   },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ///////////////////////////////////////////////////// LAYOUT / DESIGN //////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		/**
		 * Set Step Height
		 * ---------------
		 * Sets the max-height of the dom handle passed in
		 *
		 * @param jQuery step A dom handle to one of the main accordion step dds
		 */
		 setStepHeight: function($step){
		 	var total = 0;
		 	$step.children().each(function(){
		 		total += $(this).outerHeight(true);
		 	});
		 	total += total * 0.1;
		 	$step.css('max-height',total+'px');
		 },
		 /**
		  * Reset Heights
		  * -------------
		  * Resets all heights by triggering a window resize event.
		  *
		  * @param  Int	time	The number of milliseconds to wait before
		  * triggering a window resize.
		  */
		  resetHeights: function(time){
		  	setTimeout(function(){$(window).trigger('resize')},time);
		  },
		 /**
		  * Reset Selection
		  * ---------------
		  * Resets all options to default.
		  */
		  resetProductSelection: function(){
		  	var app = this;
		  	$('[data-type').removeClass('selected');
		  	for(var step in this.stepTitle){
		  		app.unsetProductSelection(step);
		  		app.unsetSelectedProduct(step);
		  	}
		  	$('select').children().eq(0).prop('selected','selected').attr('selected','selected');
		  	$('.stats input[type="radio"]').prop('checked',false);
		  	$('.stats li.active').removeClass('active');
		  	this.selectedItems = {};
		  	this.initPrices();
		  	this.resetIncompatibles();
		  },
		 /**
		  * Output Image
		  * ------------
		  * Outputs an image to the wetroom display.
		  *
		  * @param  String	filename	The name of the file to ouptut.
		  * @param  String	classId	The product type of the image.
		  */
		  outputImage: function(filename,classId){
		  	var $elem = $('.' + classId,this.kitDisplay);
		  	$elem.children('img').attr('src',this.url.imgSrc + classId + "/" + filename + '.png');
		  	if(filename == 'default'){
		  		$elem.addClass('inactive');
		  		$elem.find('p').removeClass('hidden');
		  	}
		  	else {
		  		$elem.removeClass('inactive');
		  		$elem.find('p').addClass('hidden');
		  	}
		  	this.outputtedImages[classId] = filename;
		  	this.setCookies();
		  },
		  /**
		   * Activate Button
		   * ---------------
		   * Activates the nearest button.
		   *
		   * @param	jQuery	$elem	The element being passed.
		   */
		   activateButton: function($elem){
		   	$output = $elem.hasClass('price-output-block') ? $elem : this.getPriceOutputBlock($elem);
		   	$output.find('button').addClass('active');
		   },
		  /**
		   * Deactivate Button
		   * -----------------
		   * Deactivates the nearest button.
		   *
		   * @param	jQuery	$elem	The element being passed.
		   */

		   deactivateButton: function($elem){
		   	$output = $elem.hasClass('price-output-block') ? $elem : this.getPriceOutputBlock($elem);
		   	$output.find('button').removeClass('active');
		   },
		   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  ///////////////////////////////////////////////////////// C🍪🍪KIES ////////////////////////////////////////////////////////
		 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		 /**
		  * Set Cookies
		  * -----------
		  * Iterate over the cookies array and set all of the cookies to their value
		  * particular value on the main object.
		  */
		  setCookies: function(){
		  	for(var id in this.cookiesObjs){
		  		this.cookiesObjs[id].set(this[this.cookiesObjs[id].value]);
		  	}
		  },
		 /**
		  * Unset Cookies
		  * -------------
		  * Iterate over the cookies array and unset all of the cookies.
		  */
		  unsetCookies: function(){
		  	for(var id in this.cookiesObjs){
		  		this.cookiesObjs[id].unset();
		  	}
		  },
		 /**
		  * Get Cookies
		  * -----------
		  * Iterate over the cookies array and set the value of the cookie back
		  * into the main objects value.
		  */
		  getCookies: function(){
		  	for(var id in this.cookiesObjs){
		  		this[this.cookiesObjs[id].value] = this.cookiesObjs[id].get();
		  	}
		  },
		 /**
		  * Do Cookies Exist
		  * ----------------
		  * Iterate over the cookies array and confirm that every cookie is set.
		  *
		  * @return	Boolean	Confirmation as to whether all the cookies exist.
		  */
		  doCookiesExist: function(){
		  	var proceed = true;
		  	for(var id in this.cookiesObjs){
		  		if(!this.cookiesObjs[id].doIExist()){
		  			proceed = false;
		  		}
		  	}
		  	return proceed;
		  },
		 /**
		  * Post Cookie Render
		  * ------------------
		  * Restore the program state with the newly implemented cookie data.
		  */
		  renderPageAfterCookieLoad: function(){
		  	var app = this;
		 	// Restore the images that have been outputted
		 	for(var classId in app.outputtedImages){
		 		app.outputImage(app.outputtedImages[classId],classId);
		 	}
		 	// Restore the selections that have been made
		 	for(var key in app.selectedItems)
		 	{
		 		$elem = $j('[data-value="' + app.selectedItems[key] + '"]');
		 		if($elem.is('option')){
		 			$elem.prop('selected','selected').attr('selected','selected')
		 		}
		 		else {
		 			app.setActiveGroupItem($elem);
		 		}
		 	}
		 	// Restore the step selections and re-activate the buttons
		 	for(var step in app.selectedProducts){
		 		// For all static products
		 		if("static" in app.selectedProducts[step]){
		 			$(this.kitItems).each(function(index,item){
		 				// If we have previously added it to our kit then output the selection name
		 				if(item.sku == app.selectedProducts[step]['sku']){
		 					app.setProductSelection(step,app.selectedProducts[step]);
		 				}
		 			});
		 		}
		 		// Otherwise just output the selection names and activate the buttons
		 		else {
		 			app.setProductSelection(step,app.selectedProducts[step]);
		 			app.activateButton(app.getPriceOutputBlock($('[data-step="' + step + '"]')));
		 		}
		 	}
		 	// Restore the locked / unlocked state of each step
		 	for(var step in app.unlockedSteps){
		 		app.unlockStep(step);
		 	}
		 	// Restore the completed state of each step and resolve any step dependencies
		 	for(step in app.completedSteps){
		 		app.stepTitle[step].addClass('completed');
		 		app.resolveDependencies(app.stepBody[step]);
		 	}
		 	// Re-render the kit items list and reset the incompatibilities
		 	this.renderKitSelectionBlock();
		 	this.renderPageAfterRoomDimensions();
		 	this.resetIncompatibles();
		 }
		}

		$(document).ready(function(){
			mywetroom.init();
		})

		$(window).resize(function(){
			mywetroom.setDisplayHeight();
			$('dt.active + dd, [data-popup].active').each(function(){
				mywetroom.setStepHeight($(this));
			});
		})

		$(window).load(function(){
			mywetroom.scrollScreenToPosition(0);
		});

	})(jQuery);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

(function($){
/**
	 * Room Calculator Object
	 * ----------------------
	 * Object containing the specific funcitonality for the room calculator,
	 * used in the calculate your kit section of the mywetroom.
	 */
	 roomcalc = {
	 	init: function(){
	 		var app = this;
	 		this.getHandles();
	 		this.calc.on('keyup','input',function(){
	 			roomcalc.calculate();
	 		});
	 		this.steptwo.on('click','a',function(){
	 			roomcalc.addFixture();
	 		});
	 		this.footprints.on('click','.entry span.delete',function(){
	 			$(this).closest('.entry').remove();
	 			roomcalc.calculate();
	 		});
	 		$('.step-link[data-room-step]').bind('click',function(){
	 			var step = $(this).data('room-step')
	 			$('li.step-link').removeClass('active');
	 			$('li.step-link[data-room-step="' + step + '"]').addClass('active');
	 			$('.step-output').removeClass('active');
	 			$('.step-output[data-room-step="' + step + '"]').addClass('active');
	 			$(window).trigger('resize');
	 		});
	 		this.stepthree.find('button').bind('click',function(){
	 			if(app.finalFigure > 0){
	 				app.calculatedKitSize.addClass('active')
	 				app.calculatedKitSize.children('span').html(app.finalFigure + 'm<sup>2</sup>');
	 				mywetroom.scrollScreenToPosition($('.lengths').offset().top);
	 			}
	 		});
	 	},
	 	getHandles: function(){
	 		this.calc = $('#room-calculator');
	 		this.stepone = this.calc.find('.step-output[data-room-step="one"]');
	 		this.steptwo = this.calc.find('.step-output[data-room-step="two"]');
	 		this.stepthree = this.calc.find('.results');
	 		this.footprints = this.steptwo.find('.footprints');
	 		this.calculatedKitSize = $('.calculated-kit-size');
	 		this.mobileTabs = $('.steps-list');
	 		this.finalFigure = 0;
	 	},
	 	calculate: function(){
	 		var totalRoomSize = this.stepone.find('input.length').val() * this.stepone.find('input.width').val();
	 		var totalFixedFurniture = 0;
	 		$('.footprints .entry').each(function(index,entry){
	 			totalFixedFurniture += $(entry).find('input.length').val() * $(entry).find('input.width').val();
	 		})
	 		var totalHeatingArea = totalRoomSize - totalFixedFurniture;
	 		if(totalHeatingArea != 0){
	 			this.stepthree.removeClass('hidden');
	 			$(window).trigger('resize');
	 		}
	 		if(totalFixedFurniture == 0){
	 			this.stepthree.find('.fixed').css('color','#f00');
	 		}
	 		else {
	 			this.stepthree.find('.fixed').css('color','');
	 		}
	 		this.stepthree.find('.size').html(parseFloat(this.outputValue(totalRoomSize)).toFixed(2) + 'm<sup>2</sup>');
	 		this.stepthree.find('.fixed').html('- ' + parseFloat(this.outputValue(totalFixedFurniture)).toFixed(2) + 'm<sup>2</sup>');
	 		this.stepthree.find('.heating').html('= ' + parseFloat(this.outputValue(totalHeatingArea)).toFixed(2) + 'm<sup>2</sup>');
	 		this.stepthree.find('.less').html('- ' + parseFloat(this.outputValue(totalHeatingArea) * 0.1).toFixed(2) + 'm<sup>2</sup>');
	 		var outputValue = parseFloat(totalHeatingArea * 0.9);
	 		var totalHeatingArea = outputValue < 5 ? (Math.floor(outputValue * 2) / 2).toFixed(1) : outputValue.toFixed(0);
	 		this.stepthree.find('.final').children('span').html(totalHeatingArea + 'm<sup>2</sup>');
	 		this.finalFigure = totalHeatingArea;
	 	},
	 	outputValue: function(value){
	 		return value.toFixed(2).replace('.00','').trim();
	 	},
	 	addFixture: function(){
	 		var first = this.footprints.children('.entry').length <= 0 ? 1 : 0;
	 		var entry = $('<div class="entry"></div>');
	 		entry.append('<div class="input-holder">');
	 		if(first == 1){
	 			entry.children('.input-holder').append('<label>Length</label>');
	 		}
	 		entry.children('.input-holder').append('<input type="number" min="0" class="length"/>');
	 		entry.children('.input-holder').append('<span class="after">m</span>');
	 		entry.append('<span class="input-sep">X</span>');
	 		entry.append('<div class="input-holder">');
	 		if(first == 1){
	 			entry.children('.input-holder').last().append('<label>Width</label>');
	 		}
	 		entry.children('.input-holder').last().append('<input type="number" min="0" class="width"/>');
	 		entry.children('.input-holder').last().append('<span class="after">m</span>');
	 		entry.append('<span class="delete"></span>');
	 		this.footprints.append(entry);
	 		var bottomValue = this.footprints.children('.entry').length * this.footprints.find('.entry').last().height();
	 		this.footprints.animate({
	 			scrollTop: bottomValue
	 		},500)
	 		$(window).trigger('resize');
	 	}
	 }
	 $(document).ready(function(){
	 	roomcalc.init();
	 });

	})(jQuery);
