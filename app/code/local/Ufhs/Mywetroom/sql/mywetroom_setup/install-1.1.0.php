<?php
/**
 * @version     $Id$
 * @package     Ufhs_Mywetroom
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

$installer = $this;

$installer->startSetup();

$installer->run("
	DROP TABLE IF EXISTS {$installer->getTable('mywetroom/log')};

	CREATE TABLE {$installer->getTable('mywetroom/log')} (
	`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`kititems` VARCHAR(4000) NOT NULL DEFAULT '',
	`quoteid` int(10) UNSIGNED NOT NULL DEFAULT 0,
	`datetime` DATETIME DEFAULT NULL,
	`total` FLOAT NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
	)
	COLLATE='latin1_swedish_ci'
	ENGINE=InnoDB
	;
	");

$installer->endSetup();