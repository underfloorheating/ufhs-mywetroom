<?php

/**
 * @version $Id$
 * @package Ufhs_Mywetroom
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Mywetroom_Block_Lengths extends Mage_Core_Block_Template
{
	public $lengths = [];
	public function __construct()
	{
		parent::__construct();
		$product = Mage::getSingleton('catalog/product')->loadByAttribute('sku','150WMAT');
		$selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection(
			$product->getTypeInstance(true)->getOptionsIds($product), $product
			);
		foreach($selectionCollection as $option)
		{
			if($option->getOptionId() == 6)
			{
				$this->lengths[$option->getSku()] = ['name' => $option->getName()];
			}			
		}
	}
}