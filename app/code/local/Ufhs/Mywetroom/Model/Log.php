<?php

/**
 *
 * @version     $Id$
 * @package     Ufhs_Mywetroom
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_Mywetroom_Model_Log extends Mage_Core_Model_Abstract
{
	public function _construct()
    {
        parent::_construct();
        $this->_init('mywetroom/log');
    }

}