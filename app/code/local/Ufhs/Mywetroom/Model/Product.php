<?php

/**
 * @version $Id$
 * @package Ufhs_Mywetroom
 * @author Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */

class Ufhs_Mywetroom_Model_Product extends Mage_Core_Model_Abstract
{
	public function __construct()
	{
		parent::__construct();
		$this->getQuote();
	}

	private $cart;
	private $quote;

	private $selectionTable = [
	'centre|800x800|20' => 'CENTREDRAIN800X800X20',
	'centre|900x900|20' => 'CENTREDRAIN900X900X20',
	'centre|1000x1000|20' => 'CENTREDRAIN1000X1000X20',
	'centre|1200x900|20' => 'CENTREDRAIN1200X900X20',
	'corner|800x800|20' => 'CORNERDRAIN800X800X20',
	'corner|900x900|20' => 'CORNERDRAIN900X900X20',
	'corner|1000x1000|20' => 'CORNERDRAIN1000X1000X20',
	'corner|1200x900|20' => 'CORNERDRAIN1200X900X20',
	'centre|800x800|30' => 'CENTREDRAIN800X800',
	'centre|900x900|30' => 'CENTREDRAIN900X900',
	'centre|1000x1000|30' => 'CENTREDRAIN1000X1000',
	'centre|1200x900|30' => 'CENTREDRAIN1200X900',
	'centre|1500x800|30' => 'CENTREDRAIN1500X800',
	'centre|1600x900|30' => 'CENTREDRAIN1600X900',
	'centre|1200x1200|30' => 'CENTREDRAIN1200X1200',
	'centre|1850x900|30' => 'CENTREDRAIN1850X900',
	'corner|800x800|30' => 'CORNERDRAIN800X800',
	'corner|900x900|30' => 'CORNERDRAIN900X900',
	'corner|1000x1000|30' => 'CORNERDRAIN1000X1000',
	'corner|1200x900|30' => 'CORNERDRAIN1200X900',
	'corner|1200x1200|30' => 'CORNERDRAIN1200X1200',
	'end|1200x900|30' => 'ENDDRAIN1200X900',
	'end|1500x800|30' => 'ENDDRAIN1500X800',
	'end|1600x900|30' => 'ENDDRAIN1600X900',
	'linearCentre|900x900|30' => 'LINEARCENTREDRAIN900X900',
	'linearCentre|1200x900|30' => 'LINEARCENTREDRAIN1200X900',
	'linearCentre|1400x900|30' => 'LINEARCENTREDRAIN1400X900',
	'linearCentre|1600x900|30' => 'LINEARCENTREDRAIN1600X900',
	'linearEnd|900x900|30' => 'LINEARENDDRAIN900X900',
	'linearEnd|1200x900|30' => 'LINEARENDDRAIN1200X900',
	'linearEnd|1600x900|30' => 'LINEARENDDRAIN1600X900',
	'linearEnd|1400x900|30' => 'LINEARENDDRAIN1400X900'
	];

	private $imageTable = [
	'CENTREDRAIN800X800X20' => ['class' => 'tray','filename' => '20-centre'],
	'CENTREDRAIN900X900X20' => ['class' => 'tray','filename' => '20-centre'],
	'CENTREDRAIN1000X1000X20' => ['class' => 'tray','filename' => '20-centre'],
	'CENTREDRAIN1200X900X20' => ['class' => 'tray','filename' => '20-centre'],
	'CORNERDRAIN800X800X20' => ['class' => 'tray','filename' => '20-corner'],
	'CORNERDRAIN900X900X20' => ['class' => 'tray','filename' => '20-corner'],
	'CORNERDRAIN1000X1000X20' => ['class' => 'tray','filename' => '20-corner'],
	'CORNERDRAIN1200X900X20' => ['class' => 'tray','filename' => '20-corner'],
	'CENTREDRAIN800X800' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN900X900' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1000X1000' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1200X900' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1500X800' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1600X900' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1200X1200' => ['class' => 'tray','filename' => '30-centre'],
	'CENTREDRAIN1850X900' => ['class' => 'tray','filename' => '30-centre'],
	'CORNERDRAIN800X800' => ['class' => 'tray','filename' => '30-corner'],
	'CORNERDRAIN900X900' => ['class' => 'tray','filename' => '30-corner'],
	'CORNERDRAIN1000X1000' => ['class' => 'tray','filename' => '30-corner'],
	'CORNERDRAIN1200X900' => ['class' => 'tray','filename' => '30-corner'],
	'CORNERDRAIN1200X1200' => ['class' => 'tray','filename' => '30-corner'],
	'ENDDRAIN1200X900' => ['class' => 'tray','filename' => '30-end'],
	'ENDDRAIN1500X800' => ['class' => 'tray','filename' => '30-end'],
	'ENDDRAIN1600X900' => ['class' => 'tray','filename' => '30-end'],
	'LINEARCENTREDRAIN900X900' => ['class' => 'tray','filename' => '30-linearCentre'],
	'LINEARCENTREDRAIN1200X900' => ['class' => 'tray','filename' => '30-linearCentre'],
	'LINEARCENTREDRAIN1400X900' => ['class' => 'tray','filename' => '30-linearCentre'],
	'LINEARCENTREDRAIN1600X900' => ['class' => 'tray','filename' => '30-linearCentre'],
	'LINEARENDDRAIN900X900' => ['class' => 'tray','filename' => '30-linearEnd'],
	'LINEARENDDRAIN1200X900' => ['class' => 'tray','filename' => '30-linearEnd'],
	'LINEARENDDRAIN1600X900' => ['class' => 'tray','filename' => '30-linearEnd'],
	'LINEARENDDRAIN1400X900' => ['class' => 'tray','filename' => '30-linearEnd'],
	'STANDARDHORIZONTALDRAIN' =>['class' => 'drain','filename' => 'standard'],
	'STANDARDDRAINSTAINLESS' =>['class' => 'drain','filename' => 'stainless'],
	'STANDARDDRAINTILEABLE' =>['class' => 'drain','filename' => 'tileable'],
	'LINEARDRAINSTAINLESS' =>['class' => 'drain','filename' => 'linearStainless'],
	'LINEARDRAINTILEABLE' =>['class' => 'drain','filename' => 'linearTileable'],
	'150W1M' => ['class' => 'mat','filename' => 'single'],
	'150W1.5M' => ['class' => 'mat','filename' => 'single'],
	'150W2M' => ['class' => 'mat','filename' => 'single'],
	'150W2.5M' => ['class' => 'mat','filename' => 'single'],
	'150W3M' => ['class' => 'mat','filename' => 'single'],
	'150W3.5M' => ['class' => 'mat','filename' => 'single'],
	'150W4M' => ['class' => 'mat','filename' => 'single'],
	'150W4.5M' => ['class' => 'mat','filename' => 'single'],
	'150W5M' => ['class' => 'mat','filename' => 'single'],
	'150W6M' => ['class' => 'mat','filename' => 'single'],
	'150W7M' => ['class' => 'mat','filename' => 'single'],
	'150W8M' => ['class' => 'mat','filename' => 'single'],
	'150W9M' => ['class' => 'mat','filename' => 'single'],
	'150W10M' => ['class' => 'mat','filename' => 'single'],
	'150W11M' => ['class' => 'mat','filename' => 'single'],
	'150W12M' => ['class' => 'mat','filename' => 'single'],
	'150W13M' => ['class' => 'mat','filename' => 'double'],
	'150W14M' => ['class' => 'mat','filename' => 'double'],
	'150W15M' => ['class' => 'mat','filename' => 'double'],
	'150W16M' => ['class' => 'mat','filename' => 'double'],
	'150W17M' => ['class' => 'mat','filename' => 'double'],
	'150W18M' => ['class' => 'mat','filename' => 'double'],
	'150W19M' => ['class' => 'mat','filename' => 'double'],
	'150W20M' => ['class' => 'mat','filename' => 'double'],
	'150W21M' => ['class' => 'mat','filename' => 'double'],
	'150W22M' => ['class' => 'mat','filename' => 'double'],
	'150W23M' => ['class' => 'mat','filename' => 'double'],
	'150W24M' => ['class' => 'mat','filename' => 'double'],
	'TR3100' =>['class' => 'stat','filename' => 'tr3100'],
	'TR3100REMOTE' =>['class' => 'stat','filename' => 'tr3100remote'],
	'TR3100REMOTESILVER' =>['class' => 'stat','filename' => 'tr3100remotesilver'],
	'TOUCHWHITE' =>['class' => 'stat','filename' => 'touchwhite'],
	'TOUCHSILVER' =>['class' => 'stat','filename' => 'touchsilver'],
	'PROWARM3IECC' =>['class' => 'stat','filename' => 'prowarm3iecc'],
	'PROWARM3IEPB' =>['class' => 'stat','filename' => 'prowarm3iepb'],
	'PROIQ-WHITE' =>['class' => 'stat','filename' => 'proiq-white'],
	'PROIQ-BLACK' =>['class' => 'stat','filename' => 'proiq-black'],
	'PROIQ-GREY' =>['class' => 'stat','filename' => 'proiq-grey']
	];

	/**
	 * Get Cart
	 * --------
	 * Either load the cart from Magento or from this class if its been
	 * previously loaded, then init the cart.
	 *
	 * @return Object A copy of the cart.
	 */
	private function _getCart ()
	{
		$this->cart = Mage::getSingleton('checkout/cart');
		$this->cart->init();
		return $this->cart;
	}

	/**
	 * Get Quote
	 * ---------
	 * Get the quote from this class's cart object, if it is a new quote, save a
	 * new quote object form the cart.
	 *
	 * @return Object A handle to the quote.
	 */
	public function getQuote()
	{
		$cart = $this->_getCart();
		$this->quote = $cart->getQuote();
		if(!$this->quote->getId())
		{
			$cart->saveQuote();
		}
		return $this->quote;
	}

	/**
	 * Add Products To Basket
	 * ----------------------
	 * Take the array of kit items and add them to the Magento basket.
	 *
	 * @param Array $kitItems An array containing the kit items.
	 */
	public function addProductsToBasket(Array $kitItems)
	{
		foreach($kitItems as $item)
		{
			$product = Mage::getModel('catalog/product')->load($item->id);
			if(!$product->getId())
			{
				continue;
			}
			$params = new Varien_Object([
				'id' => $item->id,
				'qty' => $item->qty,
				'name' => $product->getName(),
				'related_product' => ''
				]);
			$temp = array();
			if(isset($item->bundleOption))
			{
				foreach((array) $item->bundleOption as $key => $value)
				{
					$temp[$key] = $value;
				}
				$params['bundle_option'] = $temp;
			}
			else if(isset($item->superAttribute))
			{
				foreach((array) $item->superAttribute as $key => $value)
				{
					$temp[$key] = $value;
				}
				$params['super_attribute'] = $temp;
				$params['cpid'] = $this->_getParentId($item->id);
			}
			unset($temp);
			$return = $this->quote->addProduct($product,$params);
			if(!$return instanceof Mage_Sales_Model_Quote_Item)
			{
				throw new Exception($return);
				// OUTPUT $return as an ERROR in error handling extension with a copy of the product / mywetroom.kit entry and wihtout throwing
			}
		}
	}

	/**
	 * Recalculate cart Totals
	 * -----------------------
	 * Force the Magento cart to recalculate its totals.
	 */
	public function recalculateCartTotals()
	{
		$this->quote->setTotalsCollectedFlag(false)->collectTotals()->save();
		Mage::getModel('core/session')->setCartWasUpdated(true);
	}

	/**
	 * Log Purchase
	 * ------------
	 * Store a record of the purchase in the DB
	 */
	public function logPurchase($data,$total)
	{
		$tableExists = Mage::getSingleton('core/resource')
		->getConnection('core_read')
		->isTableExists('mywetroom_log',Mage::getConfig()->getResourceConnectionConfig("default_setup")->dbname);

		if($tableExists)
		{
			$db = Mage::getModel('mywetroom/log');
			$db->setData(
				array(
					'kititems' => urlencode($data),
					'quoteid' => $this->getQuote()->getId(),
					'datetime' => date("Y-m-d H:i:s"),
					'total' => $total
					)
				);
		}
		$db->save();
	}

	/**
	 * Output Wetroom Session Message
	 * ------------------------------
	 * Send a wetroom domain specific session message to Magento to output on the
	 * next page the user sees
	 *
	 * @param	String	$type	The type of session message that needs setting
	 * @param	String	$message	The message that needs setting
	 */
	public function outputwetroomSessionMessage($type,$message,$data=[])
	{
		switch($type)
		{
			case 'error':
				$message = new Redbox_Theme_Model_Messages(Redbox_Theme_Block_Messages::CUSTOM_ERROR,$message);
				break;
			case 'warning':
				$message = new Redbox_Theme_Model_Messages(Redbox_Theme_Block_Messages::CUSTOM_WARNING,$message);
				break;
			case 'success':
				$message = new Redbox_Theme_Model_Messages(Redbox_Theme_Block_Messages::CUSTOM_SUCCESS,$message);
				break;
			default:
				$message = new Redbox_Theme_Model_Messages(Redbox_Theme_Block_Messages::CUSTOM_NOTICE,$message);
				break;
		}
		$message->data = $data;
		$message->domain = 'mywetroom';
		Mage::getSingleton('core/session')->addMessage($message);
	}

	/**
	 * Clear Wetroom Cookies
	 * ---------------------
	 * Iterate over the cookies and unset anything prefixed with mywetroom
	 */
	public function clearWetroomCookies()
	{
		foreach($_COOKIE as $key => $value)
		{
			if(substr($key,0,9) == 'mywetroom')
			{
				unset($_COOKIE[$key]);
				setcookie($key,'',-1,'/');
				setcookie($key,'',-1,'/mywetroom');
			}
		}
	}

	/**
	 * Get Parent ID
	 * -------------
	 * Get the parent ID of the provided child product.
	 *
	 * @param  Int $childId	The child product's ID.
	 * @return Int	The parent's ID.
	 */
	private function _getParentId($childId)
	{
		return Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($childId)[0];
	}


	/**
	 * Validate Input
	 * --------------
	 * Confirm whether the parameter array contains a valid sku record.
	 *
	 * @param  Array	$paramArray	An array potentially containing a sku
	 * record.
	 * @return Boolean	Confirmation the sku record exists and is valid.
	 */
	public function isSkuValid($sku)
	{
		return preg_match('/^(\d|\w| |-|_|.)*$/',$sku);
	}


	/**
	 * Validate Mat Options
	 * --------------------
	 * Confirm whether the provided mat options contain a valid sku and length.
	 *
	 * @param  Array	$paramArray	An array containing the mat options
	 * record.
	 * @return Boolean	Confirmation the sku and length records exist and are valid.
	 */
	public function validateMatOptions($paramArray)
	{
		if(!isset($paramArray['stat']) || !isset($paramArray['length']))
		{
			return false;
		}
		if(!preg_match('/^(\d|\w| |-|_|.)*$/',$paramArray['stat']))
		{
			return false;
		}
		if(!preg_match('/^(\d|\w| |-|_|.)*$/',$paramArray['length']))
		{
			return false;
		}
		return true;
	}


	/**
	 * Get Bundle Mat JSON
	 * ---------------------
	 * Take the given product handles and return the required JSON format.
	 *
	 * @param  Object	$product	A handle to the product.
	 * @return json	JSON string containing the product details.
	 */
	public function getBundleMatJson($statProduct,$lengthProduct)
	{
		$bundleOptions = $this->_getMatAttributes($statProduct->getSku(),$lengthProduct->getSku());
		$size = str_replace("ProWarm&trade; 150w mat kit - ","",$lengthProduct->getName());
		$data = [
		'name' => $size . ' 150w Cable Mat ' . $statProduct->getName(),
		'id' => $this->loadProduct('150WMAT')->getId(),
		'sku' => $lengthProduct->getSku() . '|' . $statProduct->getSku(),
		'price' => number_format(floatval($statProduct->getPrice()) + floatval($lengthProduct->getPrice()),2),
		'bundleOption' => $bundleOptions
		];
		return json_encode($data);
	}

	/**
	 * Write JSON
	 * ----------
	 * Take the given product handle and return the required JSON format.
	 *
	 * @param  Object	$product	A handle to the product.
	 * @return json	JSON string containing the product details.
	 */
	public function getProductJson($product)
	{
		$id = $product->getId();
		$data = [
		'name' => $product->getData('name'),
		'id' => $id,
		'sku' => $product->getData('sku'),
		'price' => number_format($product->getData('price'),2)
		];
		if($this->isProductChild($id))
		{
			$parentId = $this->_getParentId($id);
			$attributes = $this->_getConfigurableAttributes($id,$parentId);
			$data['cpid'] = $parentId;
			$data['superAttribute'] = $attributes;
		}
		return json_encode($data);
	}

	/**
	 * Lookup SKU
	 * ----------
	 * Look the selection options up in the Selection Table and return the
	 * relevant SKU.
	 *
	 * @param  String $selection The selection record providedby the user.
	 * @return String	The SKU related to the selection.
	 */
	public function lookupSku($selection)
	{
		if(isset($this->selectionTable[$selection]))
		{
			return $this->selectionTable[$selection];
		}
		return false;
	}

	/**
	 * Load Product
	 * ------------
	 * Load and return a handle to the product with the sku provided.
	 *
	 * @param  String	$sku	The requested SKU.
	 * @return Object	A handle to the load product.
	 */
	public function loadProduct($sku)
	{
		return Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
	}

	/**
	 * Get Configurable Attributes
	 * ---------------------------
	 * Using the Child ID and the Parent ID, return an array containing the super
	 * attributes required to select that particular child product.
	 *
	 * @param  Int $childId  The Child ID
	 * @param  Int $parentId The Parent ID
	 * @return Array	An array containing a super attribute selection.
	 */
	private function _getConfigurableAttributes($childId,$parentId)
	{
		$temp = [];
		$product = Mage::getModel('catalog/product')->load($parentId);
		$attributes = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
		$child = Mage::getModel('catalog/product')->load($childId);
		foreach ($attributes as $attribute){
			$temp[$attribute['attribute_id']] = $child->getData($attribute['attribute_code']);
		}
		return $temp;
	}

	/**
	 * Get Mat Attribute
	 * -----------------
	 * Using the stat and mat length product, lookup the option / selection IDs
	 * a return an array with the corresponding values.
	 *
	 * @param  String	$statSku   The thermostat product's sku.
	 * @param  String	$lengthSku The mat length product's sku.
	 * @return Array	An array containing the option / selection values.
	 */
	private function _getMatAttributes($statSku,$lengthSku)
	{
		$bundleProduct = $this->loadProduct('150WMAT');
		$selectionCollection = $bundleProduct->getTypeInstance(true)->getSelectionsCollection($bundleProduct->getTypeInstance(true)->getOptionsIds($bundleProduct), $bundleProduct);
		$bundleOptions = [];
		foreach($selectionCollection as $option)
		{
			if($option->getSku() == $statSku || $option->getSku() == $lengthSku)
			{
				$bundleOptions[$option->option_id] = $option->selection_id;
			}
		}
		return $bundleOptions;
	}

	/**
	 * Is Product A Child
	 * ------------------
	 * Check to whether the provided product has a parent.
	 *
	 * @param  Int  $id	The child product ID.
	 * @return boolean	Whether the product is a child / has a parent.
	 */
	public function isProductChild($id)
	{
		return count(Mage::getResourceSingleton('catalog/product_type_configurable')->getParentIdsByChild($id)) ? true : false;
	}

	/**
	 * Lookup Filename
	 * ---------------
	 * Take a SKU/list of SKU and return the results of looking them up in the
	 * imageTable.
	 * @param  String	$sku	A SKU/list of SKUs
	 * @return Array	The results of the SKU lookup.
	 */
	public function lookupFilenames($sku)
	{
		$output = [];
		$skus = explode("|",$sku);
		foreach($skus as $sku)
		{
			if(isset($this->imageTable[$sku]))
			{
				$item = $this->imageTable[$sku];
				$output[$item['class']] = $item['filename'];
			}
		}
		return $output;
	}
}