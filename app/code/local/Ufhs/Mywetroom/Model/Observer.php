<?php
class Ufhs_Mywetroom_Model_Observer extends Mage_Core_Model_Abstract
{
	/**
	 * Observer for the customer saved event
	 * @param  [type] $observer [description]
	 * @return [type]           [description]
	 */
	public function mywetroom_outstanding_basket( $observer )
	{
		if(!Mage::app()->getRequest()->isAjax() && isset($_COOKIE['mywetroom-showmessage']))
		{
			if($_COOKIE['mywetroom-showmessage'] == 'true')
			{
				Mage::getModel('mywetroom/product')->outputwetroomSessionMessage('notice','<a href="#" class="close">*</a><img src="/skin/frontend/base/default/images/mywetroom/notice-logo.png"><div class="notice-content"><p><strong>Oops!</strong>You didn\'t add your kit items to the basket</p><a href="/mywetroom" class="cta">Go back to the MyWetroom tool</a><p>...or</p><a href="/mywetroom/product/addtobasket" data-ajax="mywetroomOutstandingBasket" class="button">Add to Basket</a></div>',['name' => 'outstandingBasket']);
			}
		}
	}
}
?>