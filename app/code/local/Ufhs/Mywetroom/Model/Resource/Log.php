<?php

/**
 * @version     $Id$
 * @package     Ufhs_Mywetroom
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Mywetroom_Model_Resource_Log extends Mage_Core_Model_Resource_Db_Abstract
{

    public function _construct()
    {
        $this->_init('mywetroom/log', 'id');
    }

}