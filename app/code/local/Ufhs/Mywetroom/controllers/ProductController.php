<?php

/**
 * MyWetroom Product Controller
 *
 * @author  Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 */
class Ufhs_Mywetroom_ProductController extends Mage_Core_Controller_Front_Action
{
	private $productModel;
	/**
	 * Product Lookup
	 * --------------
	 * Take a SKU / selection from the POST value and return the product's details.
	 *
	 * @return json	A JSON object containing details about the product.
	 */
	public function productlookupAction()
	{
		$productModel = $this->_getModel();
		if(!$sku = Mage::app()->getRequest()->getParam('sku'))
		{
			$error = 'No SKU has been provided.';
		}
		else
		{
			if(strpos($sku,'|'))
			{
				$sku = $productModel->lookupSku($sku);
			}
			if(!$productModel->isSkuValid($sku))
			{
				$error = 'Provided SKU is not valid.';
			}
			else
			{
				if($product = $productModel->loadProduct($sku))
				{
					echo $productModel->getProductJson($product);
					exit();
				}
				else
				{
					$error = "That product doesn't exist.";
				}
			}
		}
		echo json_encode(['error' => $error]);
	}

	/**
	 * Mat Lookup
	 * ------------
	 * Take the length and stat option from the POST value and return the
	 * product's details.
	 *
	 * @return json	A JSON object containing details about the product.
	 */
	public function matlookupAction()
	{
		$productModel = $this->_getModel();
		$params = Mage::app()->getRequest()->getParams();
		if(!$productModel->validateMatOptions($params))
		{
			$error = 'Provided options are not valid.';
		}
		else
		{
			$statSku = $params['stat'];
			$lengthSku = $params['length'];
			$statProduct = $productModel->loadProduct($statSku);
			$lengthProduct = $productModel->loadProduct($lengthSku);
			if($statProduct && $lengthProduct)
			{
				echo $productModel->getBundleMatJson($statProduct,$lengthProduct);
				exit();
			}
			else
			{
				$error = "Unable to load the stat or length products.";
			}
		}
		echo json_encode(['error' => $error]);
	}

	/**
	 * Get Model
	 * ---------
	 * Either load the product model from Magento or from this class if its been
	 * previously loaded.
	 *
	 * @return Object A copy of the product model.
	 */
	private function _getModel ()
	{
		if(!$this->productModel instanceof Ufhs_Mywetroom_Model_Product)
		{
			$this->productModel = Mage::getModel('mywetroom/product');
		}
		return $this->productModel;
	}

	/**
	 * Add To Basket
	 * -------------
	 * Take the basket array passed and add the contents to Magento's basket.
	 */
	public function addtobasketAction()
	{
		$productModel = $this->_getModel();
		$post = Mage::app()->getRequest()->getParams();

		// Get values for data and total either from POST of cookies
		if(empty($post))
		{
			$cookified = true;
			$data = Mage::getModel('core/cookie')->get('mywetroom-kititems');
			$total = Mage::getModel('core/cookie')->get('mywetroom-total');
		}
		else
		{
			$data = $post['data'];
			$total = $post['total'];
		}

		// Check to see we have both a data and a total value and leave if not
		if($data && $total)
		{
			// Decode the data into a kit
			$kit = json_decode($data);
			// Add products to basket, recalculate the totals and log the purchase
			try
			{
				$productModel->addProductsToBasket($kit);
			}
			catch (Exception $e)
			{
				echo $e->getMessage();
			}
			$productModel->recalculateCartTotals();
			$productModel->logPurchase($data,$total);

			// If we have added to the basket via the cookie method we need to do
			// a bit of cleaning up afterwards
			if(isset($cookified))
			{
				$productModel->clearWetroomCookies();
				$productModel->outputwetroomSessionMessage('success','<img src="/skin/frontend/base/default/images/mywetroom/notice-logo.png"><div class="notice-content"><p><strong>Success!</strong>The wetroom kit items you selected are now in your basket</p><a href="/checkout/cart" class="cta">Go to basket</a><p>',['name' => 'basketSuccess']);
			}
		}
	}

	/**
	 * How Much Money Have We Made
	 * ---------------------------
	 * This is the beginnings of a reporting function. In the long run this would
	 * be moved to the adminhtml and be converted into a single figure that sits
	 * at the top of a reporting page, 'TOTAL MONEY MSDE EVER!!!', or something
	 * equally as awesome.
	 * This function loads every entry in the log table and then loads up the associated
	 * quote and checks to see if it has been purchased. If it has, the total, relating
	 * to just the items added in the tool, are summed up and outputted, though this
	 * would be better situated in a model and return the figure.
	 */
	public function howmuchmoneyhavewemadeAction(){
		$collection = Mage::getModel('mywetroom/log')->getCollection()->addFieldToSelect('*');
		$total = 0.00;
		foreach($collection as $log)
		{
			$quote = Mage::getModel('sales/quote')->load($log->quoteid);
			if(!$quote->getIsActive())
			{
				$total += $log->total;
			}
		}
		echo $total;
	}

	/**
	 * Filename Lookup
	 * ---------------
	 * For every SKU provided (pipe delimited) do a lookup on the imageMap.
	 * Either return a JSON object containing the filenames relating to those SKUs,
	 * or an entry to say none of the SKUs exist.
	 */
	public function filenamelookupAction(){
		$productModel = $this->_getModel();
		if(!$sku = Mage::app()->getRequest()->getParam('sku'))
		{
			$error = 'No SKU has been provided.';
		}
		else
		{
			echo json_encode($productModel->lookupFilenames($sku));
			exit();
		}
		echo json_encode(['error' => $error]);
	}
}
